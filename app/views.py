from flask import render_template, redirect, url_for
from flask import session, abort, request
from flask import send_from_directory, json
from werkzeug import generate_password_hash, check_password_hash
from app.__init__ import app
from local_settings import MYSQL_USER, MYSQL_PASSWORD
from functools import wraps
import os
import base64

from flaskext.mysql import MySQL

from email_sender import send_notice_signup, send_notice_login
from email_sender import send_mail_from_admin, send_mail_user_fired
from email_sender import send_mail_user_took_book, send_mail_user_returned_book

mysql = MySQL()
app.config["MYSQL_DATABASE_USER"]	 = MYSQL_USER
app.config["MYSQL_DATABASE_PASSWORD"] = MYSQL_PASSWORD
app.config["MYSQL_DATABASE_DB"]	   = "db_library"
app.config["MYSQL_DATABASE_HOST"]	 = "localhost"
mysql.init_app(app)

img_book_file = open("book.png", "rb")
img_book = img_book_file.read()
img_book_file.close()
def_img = base64.b64encode(img_book).decode('utf8')

# let's do some magic...
def access_required(*setting_args, **setting_kwargs):
	no_args = False
	if len(setting_args) == 1 \
		and not setting_kwargs \
		and callable(setting_args[0]):
		# We were called without args
		func = setting_args[0]
		no_args = True

	def outer(func):
		@wraps(func)
		def with_params(*args, **kwargs):

			if not session.get("username"):
				return redirect(url_for("login"))

			for role, description in setting_kwargs.items():
				if role == "role" and description == "worker" and not session.get("role") == "worker":
						return render_template("error.html", error = "Не имеешь права!")
						
			for role, description in setting_kwargs.items():
				if role == "role" and description == "admin" and not session.get("role") == "admin":
						return render_template("error.html", error = "Не имеешь права!")

			return func(*args, **kwargs)
		return with_params

	if no_args:
		return outer(func)
	else:
		return outer

@app.route('/favicon.ico')
def get_favicon():
	return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route("/")
@app.route("/index")
def index():
    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute(
        "select id_book, title from books")  # TODO: replace this by procedure (column"s name will be changed)
    book_info = cursor.fetchall()


    cursor.callproc("displaysAllBooks")
    data = list(cursor.fetchall())
    if session.get("username"):
        cursor.callproc("onLoginToReturnIdUser", [session["username"], ])
        id_user = cursor.fetchone()
        for i in range(data.__len__()):
            if str(data[i][5]) == "0":
                cursor.callproc("sOpenedFormOnUserAndBook", [id_user[0], data[i][0], ])
                res = cursor.fetchone()
                if res:
                    lst = list(data[i])
                    lst[5] = 2
                    data[i] = tuple(lst)
    conn.commit()
    cursor.close()
    return render_template("index.html", title="Читайте книги!", book_info=book_info, all_book_info=data)

@app.route("/login", methods=["POST", "GET"])
def login():
	if request.method == "POST":
		details = request.form
		login = details["username"]
		password = details["password"]

		conn = mysql.connect()
		cursor = conn.cursor()
		cursor.callproc("checkLoginStatUser", [login,])
		data = cursor.fetchone()
		cursor.close()

		if str(data[0]) == "0":
			return render_template("login.html", title="Вход в систему", errorLogin="Ошибка при попытке войти в систему: неверный логин.")

		if not check_password_hash(str(data[3]), password):
			return render_template("login.html", title="Вход в систему", errorPassword="Ошибка при попытке войти в систему: неверная пара логин + пароль.");

		session["username"] = login
		session["name"] = data[1]
		if data[4] == 0:
			session["role"] = "reader"
		elif data[4] == 1:
			session["role"] = "worker"
		elif data[4] == 2:
			session["role"] = "admin"
			
		conn = mysql.connect()
		cursor = conn.cursor()
		cursor.execute("select e_mail from users where login = %s", [login,])
		e_mail = cursor.fetchone()
		cursor.execute("select first_name, last_name from users where login = %s", [login,])
		user_name = cursor.fetchone()
		cursor.close()
		
		send_notice_login(e_mail[0], user_name[0] + " " + user_name[1])

		return render_template("congratulation.html", description="Вы успешно авторизовались!")

	return render_template("login.html", title="Вход в систему")


@app.route("/signup", methods=["POST", "GET"])
def signup():
	if request.method == "POST":
		details = request.form
		login = details["login"]
		password = generate_password_hash(details["password"])

		user_first_name = details["first_name"]
		user_last_name = details["last_name"]
		user_email = details["email"]
		user_type = 0;  # reader
		user_status = 1;  # not blocked

		conn = mysql.connect()
		cursor = conn.cursor()

		cursor.callproc("checkLoginStatUser", [login, ])
		data = cursor.fetchone()
		if str(data[0]) != "0":
			cursor.close()
			return render_template("signup.html", title="Регистрация", errorLogin="Ошибка при регистрации: пользователь с таким логином уже существует.")

		cursor.callproc("addNewUser",(user_email, login, password, user_first_name, user_last_name, user_type, user_status))
		# data = cursor.fetchone()
		conn.commit()
		cursor.close()

		session["username"] = login
		session["role"] = "reader"

		# TODO: here you can send e-mail notification
		user_name = user_first_name + " " + user_last_name
		send_notice_signup(user_email, user_name, login)
		return render_template("congratulation.html", description="Вы успешно зарегистрировались!")

	return render_template("signup.html", title = "Регистрация")


@app.route("/logout")
@access_required
def logout():
    session.pop("username", None)
    session.pop("role", None)

    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute(
        "select id_book, title from books")  # TODO: replace this by procedure (column"s name will be changed)
    book_info = cursor.fetchall()

    cursor.callproc("displaysAllBooks")
    data = list(cursor.fetchall())
    conn.commit()
    cursor.close()
    return render_template("index.html", title="Читайте книги!", book_info=book_info, all_book_info=data)


@app.route("/rules")
def lib_rules():
	return render_template("rules.html", title="Несите ответственность!")


@app.route("/sendMsg", methods=["POST", "GET"])
@access_required(role = "admin")
def send_msg():
    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute("select id_user,login from users where role = 1")  # TODO: replace this by procedure (column"s name will be changed)
    user_info = cursor.fetchall()
    cursor.close()

    if request.method == "POST":
        details = request.form
        login = details["username"]
        message = details["msg"]

        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute("SELECT id_user,login FROM users WHERE role = 1 AND login=%s", [login, ])
        data = cursor.fetchone()
        cursor.close()

        if data is None:
            return render_template("sendMsg.html", title="...И будьте послушными",
        						   errorLogin="Ошибка при отправлении сообщения: неверный логин.", user_info=user_info)

        cursor = conn.cursor()
        cursor.execute("select e_mail from users where login = %s", [login,])
        e_mail = cursor.fetchone()

        cursor.execute("select first_name,last_name from users where login = %s", [login,])
        username = cursor.fetchone()

        cursor.close()

        send_mail_from_admin(e_mail[0], username[0] + " " + username[1], message)

        return render_template("congratulation.html", description="Письмо отправлено!")

    return render_template("sendMsg.html", title="...И будьте послушными", user_info=user_info)


@app.route("/addWorker", methods=["POST", "GET"])
@access_required(role = "admin")
def add_worker():
	if request.method == "POST":
		details = request.form
		login = details["login"]
		password = details["password"]
		first_name = details["first_name"]
		last_name = details["last_name"]
		email = details["email"]
		
		if not (login and password and first_name and last_name and email):
			return render_template("error.html", error="Вы заполнили не все поля.");
			
		conn = mysql.connect()
		cursor = conn.cursor()
		cursor.execute("select id_user from users where login=%s", [login,])
		response = cursor.fetchone()
		conn.commit()
		cursor.close()

		if response:
			return render_template("addWorker.html", title="Плодитесь", errorLogin="Такой пользователь уже существует.")

		user_type = 1;  # worker
		user_status = 1;  # not blocked
		password = generate_password_hash(password)
				
		conn = mysql.connect()
		cursor = conn.cursor()
		cursor.execute("insert into users (e_mail, login, password, first_name, last_name, role, state) "
						+ "values (%s, %s, %s, %s, %s, %s, %s)",
						[email, login, password, first_name, last_name, user_type, user_status,])
		response = list(cursor.fetchall())
		conn.commit()
		cursor.close()
		
		send_notice_signup(email, first_name + " " + last_name, login)
		
		return render_template("congratulation.html", description="Работник добавлен!")
	return render_template("addWorker.html", title="Плодитесь")


@app.route("/removeWorker", methods=["POST", "GET"])
@access_required(role = "admin")
def remove_worker():
    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute("select id_user,login from users where role = 1")  # TODO: replace this by procedure (column"s name will be changed)
    user_info = cursor.fetchall()
    cursor.close()

    if request.method == "POST":
        details = request.form
        login = details["username"]

        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute("SELECT id_user,login FROM users WHERE role = 1 AND login=%s", [login,])
        data = cursor.fetchone()
        cursor.close()

        if data is None:
            return render_template("removeWorker.html", title="Изыйди", errorLogin="Его уже с нами нет...", user_info=user_info)

        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute("select e_mail from users where login=%s", [login,])
        user_email = cursor.fetchone()
        cursor.execute("select first_name,last_name from users where login=%s", [login,])
        user_name = cursor.fetchone()
        cursor.execute("delete from users where login in (%s)", [login,])
        response = cursor.fetchone()
        conn.commit()
        cursor.close()

        send_mail_user_fired(user_email[0], user_name[0] + " " + user_name[1])

        return render_template("congratulation.html", description="Работник уволен!")
    return render_template("removeWorker.html", title="Изыйди", user_info=user_info)


@app.route("/myBooks")
@access_required
def my_books():
    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute(
        "select id_book, title from books")  # TODO: replace this by procedure (column"s name will be changed)
    book_info = cursor.fetchall()

    cursor.callproc("displaysAllBooks")
    data = list(cursor.fetchall())
    cursor.callproc("onLoginToReturnIdUser", [session["username"], ])
    id_user = cursor.fetchone()
    data1 = []
    for i in range(data.__len__()):
        if str(data[i][5]) == "0":
            cursor.callproc("sOpenedFormOnUserAndBook", [id_user[0], data[i][0], ])
            res = cursor.fetchone()
            if res:
                data1.append([data[i][0], data[i][1], data[i][2], data[i][3], data[i][4], data[i][5], res[3], res[4]])
    conn.commit()
    cursor.close()
    return render_template("myBooks.html", title="...!", book_info=book_info, all_book_info=data1)


@app.route("/createForm", methods=["POST", "GET"])
@access_required(role = "worker")
def create_form():
    conn = mysql.connect()
    cursor = conn.cursor()

    cursor.execute("select id_user,login from users where state = 1 and role = 0") #TODO: replace this by procedure (column"s name will be changed)
    user_info = cursor.fetchall()

    cursor.execute("select id_book,title from books") #TODO: replace this by procedure (column"s name will be changed)
    book_info = cursor.fetchall()


    cursor.execute(
    "select id_book, title, author, published, picture, status from books")  # TODO: replace this by procedure (column"s name will be changed)
    all_book_info = cursor.fetchall()

    cursor.close()

    if request.method == "POST":
        details   = request.form
        id_book   = details["result"]
        login   = details["username"]
        dateFrom  = details["dateFrom"]
        dateTo	= details["dateTo"]

        conn = mysql.connect()
        cursor = conn.cursor()

        cursor.execute("SELECT id_user,login, state FROM users WHERE role = 0 AND login=%s", [login, ])
        data = cursor.fetchone()

        if data is None:
            return render_template("createForm.html", title="Ученье - свет!", errorBook="Неверный логин",
                                   user_info=user_info, book_info=book_info, all_book_info=all_book_info);


        id_user = data[0]

        if data[2] == 0:
            return render_template("createForm.html", title="Ученье - свет!", errorBook="Выбранный читатель заблокирован",
                                   user_info=user_info, book_info=book_info, all_book_info=all_book_info);

        if id_book == "":
            return render_template("createForm.html", title="Ученье - свет!", errorBook="Вы не выбрали книгу",
                                   user_info=user_info, book_info=book_info, all_book_info=all_book_info);
        # cursor.callproc("changeStatOnIdBook", [id_book, ])
        cursor.execute("update books set status = 0 where id_book = %s", [id_book,])
        cursor.callproc("addNewForm", [id_user, id_book, dateFrom, dateTo, ])
        # data = cursor.fetchone() # TODO: можно использовать для обработки исключений с работой MySQL
        conn.commit()
        cursor.close()

        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute("select e_mail,first_name,last_name from users where id_user=%s", [id_user,])
        email_user_info = cursor.fetchone()
        cursor.execute("select title,author,published from books where id_book=%s", [id_book,])
        email_book_info = cursor.fetchone()
        cursor.close()

        email_book_info = email_book_info[0] + ", " + email_book_info[1] + ", " + str(email_book_info[2])

        send_mail_user_took_book(email_user_info[0], email_user_info[1] + " " + email_user_info[2], email_book_info, dateTo)

        return render_template("congratulation.html", description="Формуляр успешно добавлен!")

    return render_template("createForm.html", title="Ученье - свет!",
							user_info=user_info, book_info=book_info, all_book_info=all_book_info)


@app.route("/closeForm", methods=["POST", "GET"])
@access_required(role = "worker")
def close_form():
    conn = mysql.connect()
    cursor = conn.cursor()

    cursor.execute(
        "select id_user,login from users where state = 1 and role = 0")  # TODO: replace this by procedure (column"s name will be changed)
    user_info = cursor.fetchall()

    cursor.execute("select id_book,title from books")  # TODO: replace this by procedure (column"s name will be changed)
    book_info = cursor.fetchall()

    cursor.execute(
        "select id_book, title, author, published, picture, status from books")  # TODO: replace this by procedure (column"s name will be changed)
    data = cursor.fetchall()

    data1 = []
    data2 = []
    for i in range(data.__len__()):
        if str(data[i][5]) == "0":
            idBook = data[i][0]
            cursor.callproc("sOpened_", [data[i][0], ])
            res = cursor.fetchone()
            if res:
                data1.append([data[i][0], data[i][1], data[i][2], data[i][3], data[i][4], res[1]])
                data2.append([data[i][0], data[i][1], res[1]])
        else:
            data1.append([data[i][0], data[i][1], data[i][2], data[i][3], data[i][4], -1])
            data2.append([data[i][0], data[i][1], -1])

    cursor.close()

	
    if request.method == "POST":
        details = request.form
        id_book = details["result"]
        login = details["username"]
        date	= details["date"]

        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute("SELECT id_user,login,state FROM users WHERE role = 0 AND login=%s", [login, ])
        data = cursor.fetchone()

        if data is None:
            return render_template("closeForm.html", title="Успел-таки!", errorBook="Неверный логин",
                                   user_info=user_info, book_info=data2, all_book_info=data1);

        id_user = data[0]

        if id_book == "":
            return render_template("closeForm.html", title="Успел-таки!", errorBook="Вы не выбрали книгу",
                                   user_info=user_info, book_info=data2, all_book_info=data1);

        # for id_book in id_books:
        cursor.callproc("upFactualReturnBookForm", [date, id_user, id_book, ])
        conn.commit()

        if data[2] == 0:
            cursor.execute("SELECT id_form,  FROM forms WHERE id_user = %d AND d_factualReturn=NULL AND d_promisedReturn <= CURRENT_DATE()", [id_user, ])
            res = cursor.fetchall()
            if res is None:
                cursor.execute("UPDATE users SET state=1 WHERE id_user=%s", [id_user, ])

        cursor.close()

        #TODO: send mail about returned book
        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute("select e_mail,first_name,last_name from users where id_user=%s", [id_user,])
        email_user_info = cursor.fetchone()
        cursor.execute("select title,author,published from books where id_book=%s", [id_book,])
        email_book_info = cursor.fetchone()
        cursor.close()

        email_book_info = email_book_info[0] + ", " + email_book_info[1] + ", " + str(email_book_info[2])
        send_mail_user_returned_book(email_user_info[0], email_user_info[1] + " " + email_user_info[2], email_book_info, date)

        return render_template("congratulation.html", description="Формуляр успешно закрыт!")
		
    return render_template("closeForm.html", title="Успел-таки!",
							user_info=user_info, book_info=data2, all_book_info=data1)

@app.route("/addBooks", methods=["POST", "GET"])
@access_required(role = "worker")
def addBooks():
    if request.method == "POST":
        details = request.form
        book_name = details["book_name"]
        book_author = details["book_author"]
        book_published = details["book_published"]
        book_description = details["book_description"]
        book_count = details["book_count"]

        book_picture = request.files["pic"]
        if not book_picture:
            imgStr = base64.b64encode(img_book).decode('utf8')
        else:
            files = {"pic": book_picture.read()}
            imgStr = base64.b64encode(files["pic"]).decode('utf8')

        # my_file = open("lastAddedBook.png", "wb")
        # my_file.write(imgByteStr)
        # my_file.close()


        conn = mysql.connect()
        cursor = conn.cursor()
        for i in range(int(book_count)):
            cursor.callproc("addNewBook", [book_name, book_author, book_published, book_description, imgStr, ])

        data = cursor.fetchone() # TODO: можно использовать для обработки исключений с работой MySQL
        conn.commit()
        cursor.close()
        return render_template("congratulation.html", description="Книги успешно добавлены!")

    return render_template("addBooks.html", title="Больше книг!!!", def_img_book=def_img)


@app.route("/deleteBooks", methods=["POST", "GET"])
@access_required(role = "worker")
def deleteBooks():
	conn = mysql.connect()
	cursor = conn.cursor()
	cursor.execute("select id_book, title from books") #TODO: replace this by procedure (column"s name will be changed)
	book_info = cursor.fetchall()

	cursor = conn.cursor()
	cursor.execute("select id_book, title, author, published, picture, status from books")  # TODO: replace this by procedure (column"s name will be changed)
	all_book_info = cursor.fetchall()
	cursor.close()
	
	if request.method == "POST":
		details = request.form
		result = details["result"]
		
		if not result:
			return render_template("error.html", error = "Вы не ввели название книги!")

		books_id_list = list(map(int, result.split()))

		conn = mysql.connect()
		cursor = conn.cursor()

		for id_book in books_id_list:
			cursor.callproc("removeOnIdBook", [id_book, ])
			conn.commit()
		cursor.close()
		
		return render_template("congratulation.html", description="Книги успешно списаны!")

	return render_template("deleteBooks.html", title=":(",
							book_info=book_info, all_book_info=all_book_info)

@app.errorhandler(404)
def page_not_found(e):
    # note that we set the 404 status explicitly
    return render_template("error.html", error="Куда путь держишь, бродяга? Ты ошибся адресом, тебе здесь не рады..."), 404

@app.errorhandler(500)
def page_not_found(e):
    # note that we set the 500 status explicitly
    return render_template("error.html", error="Библиотекари кажется сломали интернет, они просто удалили его с рабочего стола..."), 500