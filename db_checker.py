from flask import Flask
from flaskext.mysql import MySQL
from local_settings import MYSQL_USER, MYSQL_PASSWORD

from datetime import date, datetime

import schedule
import time
import sys

from email_sender import send_notice_week_leftover, send_notice_debt

app = Flask(__name__)

mysql = MySQL()
app.config["MYSQL_DATABASE_USER"]     = MYSQL_USER
app.config["MYSQL_DATABASE_PASSWORD"] = MYSQL_PASSWORD
app.config["MYSQL_DATABASE_DB"]       = "db_library"
app.config["MYSQL_DATABASE_HOST"]     = "localhost"
mysql.init_app(app)

def check_users():
	print(datetime.now(), ": START check_users")
	conn = mysql.connect()
	cursor = conn.cursor()
	cursor.execute("select id_user,id_book,d_promisedReturn from forms")
	form_info = cursor.fetchall()
	cursor.close()

	for id_user,id_book,term in form_info:
		if (0 <= (term - now).days <= 7):
			conn = mysql.connect()
			cursor = conn.cursor()

			cursor.execute("select first_name,last_name,e_mail from users where id_user = %s", (id_user, ))
			user_info = cursor.fetchone()
			
			#using fetchone because every taken book by user declared in different forms
			cursor.execute("select author,title from books where id_book = %s", (id_book, ))
			book_info = cursor.fetchone()
			cursor.close()
			
			user_email = user_info[2]
			user_name  = user_info[0] + " " + user_info[1]
			book_name  = book_info[0] + " -- " + book_info[1]
			
			print("\t" + user_name, " имеет на руках книгу \"", book_name, "\" у него осталось чуть меньше недели до срока сдачи.")

			send_notice_week_leftover(user_email, user_name, book_name, (term - now).days)
			time.sleep(10) #delay 10 seconds to prevent spam systems
			
		elif ((term - now).days < 0):
			conn = mysql.connect()
			cursor = conn.cursor()
			
			cursor.execute("select first_name,last_name,e_mail from users where id_user = %s", (id_user, ))
			user_info = cursor.fetchone()
			
			#using fetchone because every taken book by user declared in different forms
			cursor.execute("select author,title from books where id_book = %s", (id_book, ))
			book_info = cursor.fetchone()
			
			user_email = user_info[2]
			user_name  = user_info[0] + " " + user_info[1]
			book_name  = book_info[0] + " -- " + book_info[1]
			
			print("\t" + user_name, " не сдал книгу \"", book_name, "\" обещанная дата сдачи -- ", term, " БЛОКИРУЕМ!")
			
			cursor.execute("update users set state = 0 where id_user = %s", (id_user, ))
			conn.commit()
			cursor.close()

			send_notice_debt(user_email, user_name, book_name, str(term))
			time.sleep(10) #delay 10 seconds to prevent spam systems
			
	print(datetime.now(), ": END check_users")


schedule.every(1).minutes.do(check_users)
#schedule.every().day.at("08:00").do(check_users)

if __name__ == "__main__":
	now = date.today()
	try:
		while 2015:
			schedule.run_pending()
			#time.sleep(1)
	except KeyboardInterrupt:
		print("Goodbye!")
		sys.exit()