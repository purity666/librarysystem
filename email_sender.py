from flask import Flask
from flask_mail import Mail, Message

from threading import Thread

def threading(f):
    def wrapper(*args, **kwargs):
        thr = Thread(target=f, args=args, kwargs=kwargs)
        thr.start()
    return wrapper

mail_settings = {
	"MAIL_DEBUG": False,
    "MAIL_SERVER": "smtp.yandex.ru",
    "MAIL_PORT": 465,
    "MAIL_USE_TLS": False,
    "MAIL_USE_SSL": True,
    "MAIL_USERNAME": "worker.of.library@yandex.ru",
	"MAIL_NAME": "Сотрудники Библиотеки",
    "MAIL_PASSWORD": "e^3%6%XLKdc6z8x@",
	"MAIL_SUPPRESS_SEND": False,
	"TESTING": False
}

app = Flask(__name__) #как бы понятно, но лучше разобраться
app.config.update(mail_settings)
mail = Mail(app)

def send_notice_week_leftover(user_email, user_name, book_name, days):
	with app.app_context():
		msg = Message(subject="Уведомление из Библиотеки: осталось меньше недели до срока сдачи литературы!",
						sender=app.config.get("MAIL_NAME") + "<" +app.config.get("MAIL_USERNAME") + ">",
						recipients=user_email.split()
					)
		
		msg.body = "Уважаемый " + user_name + ",\n\n"
		msg.body += "приводим Вас к сведенью, что Вы имеете на руках книгу "
		msg.body += book_name + ".\n"
		msg.body += "Просим Вас заблаговременно вернуть книгу в библиотеку. В противном случае Вы не сможете брать новые книги.\n" 
		msg.body += "У Вас осталось " + str(days) + " дней\n\n"
		msg.body += "С уважением,\nСотрудники Библиотеки"
		# print(msg)
		try:
			mail.send(msg)
			print("[ Sent e-mail message for ", user_email, "]")
		except Exception as e:
			print(e)

def send_notice_debt(user_email, user_name, book_name, day_return):
	with app.app_context():
		msg = Message(subject="Уведомление из Библиотеки: книга не сдана в срок!",
						sender=app.config.get("MAIL_NAME") + "<" +app.config.get("MAIL_USERNAME") + ">",
						recipients=user_email.split()
					)
		
		msg.body = "Уважаемый " + user_name + ",\n\n"
		msg.body += "приводим Вас к сведенью, что Вы имеете на руках книгу "
		msg.body += book_name + ", которую не вернули в библиотеку к указанному сроку (" + day_return + ").\n"
		msg.body += "Просим Вас немедленно вернуть книгу в библиотеку. В противном случае Вы не сможете брать новые книги и будете должны заплатить штраф.\n\n" 
		msg.body += "С уважением,\nСотрудники Библиотеки"
		# print(msg)
		try:
			mail.send(msg)
			print("[ Sent e-mail message for ", user_email, "]")
		except Exception as e:
			print(e)

@threading
def send_notice_signup(user_email, user_name, user_login):
	with app.app_context():
		msg = Message(subject="Уведомление из Библиотеки: Вы успешно зарегистрировались!",
						sender=app.config.get("MAIL_NAME") + "<" +app.config.get("MAIL_USERNAME") + ">",
						recipients=user_email.split()
					)
		
		msg.body = "Уважаемый " + user_name + ",\n\n"
		msg.body += "Поздравляем с регистрацией в Информационной Системе Библиотеки!\n"
		msg.body += "Ваш логин: " + user_login + ".\n"
		msg.body += "Если Вы забыли пароль, то обратитесь к сотрудникам Библиотеки.\n" 
		msg.body += "Просим Вас внимательно ознакомиться с правилами Библиотеки и соблюдать их. При возникновении любых вопросов обращайтесь к сотрудникам Библиотеки.\n" 
		msg.body += "Читайте книги!\n" 
		msg.body += "\n\n" 
		msg.body += "С уважением,\nСотрудники Библиотеки"
		# print(msg)
		try:
			mail.send(msg)
			print("[ Sent e-mail message for ", user_email, "]")
		except Exception as e:
			print(e)
			
@threading
def send_notice_login(user_email, user_name):
	with app.app_context():
		msg = Message(subject="Уведомление из Библиотеки: Вы вошли в систему!",
						sender=app.config.get("MAIL_NAME") + "<" +app.config.get("MAIL_USERNAME") + ">",
						recipients=user_email.split()
					)
		
		msg.body = "Уважаемый " + user_name + ",\n\n"
		msg.body += "Мы обнаружили вашу авторизацию в Информационной Системе Библиотеки!\n"
		msg.body += "Просим Вас соблюдать правила Библиотеки. При возникновении любых вопросов обращайтесь к сотрудникам Библиотеки.\n" 
		msg.body += "Читайте книги!\n" 
		msg.body += "\n\n" 
		msg.body += "С уважением,\nСотрудники Библиотеки"
		# print(msg)
		try:
			mail.send(msg)
			print("[ Sent e-mail message for ", user_email, "]")
		except Exception as e:
			print(e)
			
@threading
def send_mail_from_admin(user_email, user_name, message):
	with app.app_context():
		msg = Message(subject="Уведомление из Библиотеки: Сообщение от Администратора!",
						sender=app.config.get("MAIL_NAME") + "<" +app.config.get("MAIL_USERNAME") + ">",
						recipients=user_email.split()
					)
		msg.body = message
		try:
			mail.send(msg)
			print("[ Sent e-mail message for ", user_email, "]")
		except Exception as e:
			print(e)
			
@threading
def send_mail_user_fired(user_email, user_name):
	with app.app_context():
		msg = Message(subject="Уведомление из Библиотеки: Ура, свобода!",
						sender=app.config.get("MAIL_NAME") + "<" +app.config.get("MAIL_USERNAME") + ">",
						recipients=user_email.split()
					)
		
		msg.body = "Уважаемый " + user_name + ",\n\n"
		msg.body += "Мы были очень рады сотрудничать с таким замечательным человеком!\n"
		msg.body += "Вас очень будет не хватать в нашем тихом и уютном коллективе...\n"
		msg.body += "Мы всегда рады Вас видеть, и Вы всегда можете вернуться к нам!\n" 
		msg.body += "До новых встреч!\n" 
		msg.body += "Читайте книги!\n" 
		msg.body += "\n\n" 
		msg.body += "С уважением,\nАдминистратор Библиотеки"
		# print(msg)
		try:
			mail.send(msg)
			print("[ Sent e-mail message for ", user_email, "]")
		except Exception as e:
			print(e)
			
@threading
def send_mail_user_took_book(user_email, user_name, book_info, return_date):
	with app.app_context():
		msg = Message(subject="Уведомление из Библиотеки: Вы взяли книгу!",
						sender=app.config.get("MAIL_NAME") + "<" +app.config.get("MAIL_USERNAME") + ">",
						recipients=user_email.split()
					)
		
		msg.body = "Уважаемый " + user_name + ",\n\n"
		msg.body += "мы рады оповестить Вас, что Вы взяли книгу!\n"
		msg.body += "Информация о книге:\n"
		msg.body += book_info + ".\n"
		msg.body += "Просим Вас соблюдать правила Библиотеки и возвратить книгу в указанный срок: "
		msg.body += return_date + ".\n"
 
		msg.body += "Читайте книги!\n" 
		msg.body += "\n\n" 
		msg.body += "С уважением,\nСотрудники Библиотеки"
		#print(msg)
		try:
			mail.send(msg)
			print("[ Sent e-mail message for ", user_email, "]")
		except Exception as e:
			print(e)
			
@threading
def send_mail_user_returned_book(user_email, user_name, book_info, return_date):
	with app.app_context():
		msg = Message(subject="Уведомление из Библиотеки: Вы сдали книгу!",
						sender=app.config.get("MAIL_NAME") + "<" +app.config.get("MAIL_USERNAME") + ">",
						recipients=user_email.split()
					)
		
		msg.body = "Уважаемый " + user_name + ",\n\n"
		msg.body += "мы рады оповестить Вас, что Вы сдали книгу!\n"
		msg.body += "Информация о книге:\n"
		msg.body += book_info + ".\n"
		msg.body += "Дата сдачи книги: "
		msg.body += return_date + ".\n"
 
		msg.body += "Читайте книги!\n" 
		msg.body += "\n\n" 
		msg.body += "С уважением,\nСотрудники Библиотеки"
		#print(msg)
		try:
			mail.send(msg)
			print("[ Sent e-mail message for ", user_email, "]")
		except Exception as e:
			print(e)