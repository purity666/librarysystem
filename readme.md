Как развернуть у себя веб-проект:
1) pip install -r requirements.txt
2) mysql -u имя_пользователя -p 
   password: ****
   mysql> source путь/до/файла/db_library.sql
3) python run.py
проект будет доступен по ссылке localhost

E-mail уведомления и анализ БД с блокировкой пользователей:
python db_checker.py

Запустить все сервисы сразу:
.\start_services.bat